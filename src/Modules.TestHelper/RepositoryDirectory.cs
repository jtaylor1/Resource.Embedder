﻿namespace Modules.TestHelper
{
	#region Enumerations

	public enum RepositoryDirectory
	{
		/// <summary>
		/// The directory containing common testfiles
		/// </summary>
		TestFiles,
		/// <summary>
		/// The central folder for sourcecode
		/// </summary>
		SourceCode,
	}

	#endregion Enumerations
}